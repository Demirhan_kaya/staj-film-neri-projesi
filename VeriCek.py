import requests
from bs4 import BeautifulSoup

filmler = open("Filmler.csv","w",encoding="utf-8")

for j in range(1,108):
    url = "https://www.sinemalar.com/en-iyi-filmler?page="+str(j)

    r = requests.get(url)
    soup = BeautifulSoup(r.content, "lxml")
    bilgiler = soup.find_all('span',{"class","side-info"})
    for i in bilgiler:
        print(i.text+",")
        filmler.write(i.text+",")
